
/* This is a template for buliding data sending   
   client, which sends the data to the dispatcher.
   It also receives the commands from the dispatcher.
*/

#include "dispatch.h"
#include "stdlib.h"
#include "stdio.h"

static char buf[100000];   /* send data buffer */

int main(int argc, char *argv[]) 
  {
  char * host;
  char tag[TAGSIZE+1];
  char cmd[200];
  int cmdsize;
  int rc;
  int nbytes = -1;
  int send_pos;

  host = "pcitep04.cern.ch"; //"name of the host where dispatcher runs"; 

  //host = (char *)0; /* if you run on the same host as dispatcher */

  rc = init_disp_link(host,"a TAG_CMD"); /* set the proper tag here */
  if( rc < 0 )
    exit(1); /* no connection */
  rc = send_me_always();
  if( rc < 0 )
    exit(2); /* connection is broken */

  for(;;)
    {
    /* main loop */
    // rc = wait_head(tag,&cmdsize); /* wait for arriving command */
  
    rc = check_head(tag,&cmdsize); /* just check without waiting */
    /* normally, you would use check_head */

    if( rc < 0 )
      exit(3); /* connection is broken */

    if( rc > 0 )
      { 
      /* we got the tag and size (in bytes) of incoming command */
      rc = get_data(cmd,sizeof(cmd)-1);
      /* we got the command body. If size is too big, the command tail
               will be simply lost 
      */
      if( rc < 0 )
        exit(4); /* connection is broken */
      if( cmdsize >= sizeof(cmd) )
         cmdsize = sizeof(cmd)-1;
      cmd[cmdsize] = 0;   /* the end marker */

      /* here we have:    
         command tag in variable "tag"
         command in "cmd"   
      */

      //process the command                   
      //..........................

      continue; /* let's look at next command */
      } /* end of dispatcher command processing */

    if( nbytes < 0 )
      {
      /* the buffer is ready for new data portion */

      //put something into buf
      //............

	nbytes = sizeof(buf); //  size of data in buf ( bytes! )
	send_pos = 0;
      }

    if( nbytes >= 0 )
      {
      /* the data portion sending is not finished yet */
      // rc = put_data("TAG-OF-DATA-TO-BE-SENT" /* 8 chars max */
      //        ,buf,nbytes,&send_pos);
    
      rc = put_fulldata("TAG_DATA",buf,nbytes);     
      /* put_data sends what can be sent and returns rc=0    
         if the data transfer is not finished completely.
         put_fulldata always send the whole block, waiting
         for the end of transmission
      */

      if( rc < 0 )
        exit(5); /* connection is broken */
      if( rc > 0 )
        nbytes = -1; /* data portion shipped completely */
      }

    //do something useful
    //.....................

    } /* end of main loop */
  }
