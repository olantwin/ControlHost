/*
 $Id: pat_c_send.c,v 1.3 1995/03/20 23:13:08 ruten Exp $
*/

/* This is a template for buliding data sending   
   client, which sends the data to the dispatcher.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dispatch.h"

static char buf[500000];   /* send data buffer */

int main(int argc, char *argv[]) {
  char * host = (char *)0;
  int rc;
  int nbytes = -1;
  int send_pos = -1;
  unsigned long cnt;
  long last = -1;

 
  if(argc>1 && strcmp(argv[1],"local")!=0)  host=argv[1];
 
  rc = init_disp_link(host,"");
  if( rc < 0 ) {
    if(host==NULL) host = "local host";
    printf("Error: no connection to %s\n",host);
    exit(1); /* no connection */
  }

  for(cnt=0;;cnt++) 
    {
    /* main loop */

    if( nbytes < 0 )
      {
      /* the buffer is ready for new data portion */

      *(long *)buf = ++last;
      nbytes = 100 + random()%400;

      if(cnt%100000 ==0) printf("cnt=%ld nbytes %d\n",cnt,nbytes); 
/*
fprintf(stderr,"nbytes %ld\n",nbytes);
*/
      send_pos = 0;
      }

    if( nbytes >= 0 )
      {
      /* the data portion sending is not finished yet */
      rc = put_data("DATA" /* 8 chars max */
              ,buf,nbytes,&send_pos);

      if( rc < 0 )
        exit(5); /* connection is broken */
      if( rc > 0 )
        {
        /* data portion shipped completely */
        send_pos = -1;
        nbytes = -1;
        cnt++;
        }
      else
       printf("%ld %d\n",cnt,send_pos);
      }
    } /* end of main loop */
  return 0;
}
