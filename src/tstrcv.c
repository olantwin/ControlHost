#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include "dispatch.h"

#define LIM 100

static long buf[LIM];

int main(int argc, char *argv[])
{
  char tg[TAGSIZE+1];
  int sz, rc;
  unsigned long ev;
  long last = -1;
  char *host = (char *)0;
  if(argc>1 && strcmp(argv[1],"local")!=0)  host=argv[1];
 
  rc=init_2disp_link(host,"am DATA","a CMD");
  if( rc < 0 ) {
    if(host==NULL) host = "local host";
    printf("Error: no connection to %s\n",host);
    exit(1); /* no connection */
  }

   send_me_always();
  for(ev=0;;ev++)
    {
    assert( wait_head(tg,&sz) > 0 );
    assert( get_data(buf,LIM) > 0 );
    if(ev%100000 == 0) printf("tstrcv ev=%ld\n",ev);
    if( strcmp(tg,"DATA") == 0 )
      {
      if( last+1 != buf[0] )
        fprintf(stderr,"Jump %ld %ld\n",last,buf[0]);
      last = buf[0];
      }
    if( argc > 2 )
      usleep(atoi(argv[2]));
    }
  return 0;
}
